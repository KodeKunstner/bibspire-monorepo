try {
  require('../logging/accesslog');
  const express = require('express');
  const app = express();
  const {v1} = require('./api');
  const {
    rootHandler,
    bibHandler,
    pidHandler,
    coverHandler,
    workHandler,
    objectHandler,
    collectionHandler,
    seriesHandler
  } = require('../bib/bib');

  app.disable('x-powered-by');
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
      'Access-Control-Allow-Methods',
      'OPTIONS, HEAD, GET, PUT, POST'
    );
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept'
    );
    next();
  });
  app.use('/v1/bib', bibHandler);
  app.use('/v1', v1);
  app.use('/pid', pidHandler);
  app.use('/cover', coverHandler);
  app.use('/work', workHandler);
  app.use('/object', objectHandler);
  app.use('/collection', collectionHandler);
  app.use('/series', seriesHandler);

  app.get('/bib', rootHandler);
  app.get('/', rootHandler);

  app.listen(process.env.PORT || 3210);
  console.log(
    'started BibSpire server on port',
    process.env.PORT || 3210
  );

  require('../bib/recommender_common');
  require('../bib/elastic');
  require('../bib/holding');
} catch (e) {
  console.error(e);
  require('child_process').exec('npm install', () => process.exit());
  console.error('npm install');
  require('http')
    .createServer((req, res) => res.end('maintenance'))
    .listen(process.env.PORT || 3210);
}
