const {accessLogStats} = require('../logging/accesslog');
const {bibHandler} = require('../bib/bib');
const {geoip} = require('./geoip');
const {swaggerSpec, swaggerUI} = require('./swagger');

const started = Date.now();
async function v1(req, res, next) {
  if (
    req.url.startsWith('/recommend') ||
    req.url.startsWith('/object') ||
    req.url.startsWith('/collection') ||
    req.url.startsWith('/randomPid') ||
    req.url.startsWith('/libraries')
  ) {
    bibHandler(req, res, next);
  } else if (req.url.startsWith('/geoip/')) {
    res.json(await geoip(req.url.replace('/geoip/', '')));
    res.end();
  } else if (req.url.startsWith('/stats/accesslog')) {
    res.json(
      await accessLogStats(req.url.replace('/stats/accesslog', ''))
    );
    res.end();
  } else if (req.url.startsWith('/stats/uptime')) {
    res.json((Date.now() - started) / 1000);
    res.end();
  } else if (req.url === '/oas3-spec.json') {
    await swaggerSpec(req, res);
  } else {
    swaggerUI(req, res);
  }
}
module.exports = {
  v1
};
