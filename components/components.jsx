import React from 'react';
import axios from 'axios';
import ReactDOMServer from 'react-dom/server';
import ReactDOM from 'react-dom';

const isNode = typeof window === 'undefined';
const isBrowser = !isNode;

export function WorkView(props) {
  const {work} = props;
  //console.log(work);
  return (
    <React.Fragment>
      <main
        className="content"
        style={{textAlign: 'center'}}
        itemScope="itemScope"
        itemType={'http://schema.org/' + work['@type']}
      >
        <link itemProp="url" href={work.url} />
        {work.image && (
          <img style={{margin: 20}} width="280" src={work.image} />
        )}
        <div
          style={{
            display: 'inline-block',
            maxWidth: 600,
            textAlign: 'left',
            verticalAlign: 'top',
            margin: '0 16px 44px 16px'
          }}
        >
          <h1 style={{clear: 'none'}}>
            <span itemProp="name">{work.name}</span>
            {work.alternativeHeadline && (
              <span
                className="alternativeHeadline"
                itemProp="alternativeHeadline"
              >
                {work.alternativeHeadline}
              </span>
            )}
            {work.datePublished && `(${work.datePublished})`}
          </h1>
          <p className="creatorcontributor">
            {work.creator.map(s => (
              <span key={s} className="creator" itemProp="creator">
                {s}
              </span>
            ))}
            {work.contributor.map(s => (
              <span
                key={s}
                className="creator contributor"
                itemProp="contributor"
              >
                {s}
              </span>
            ))}
          </p>
          {work.description.map(s => (
            <p key={s} className="description" itemProp="description">
              {s}
            </p>
          ))}
          <p>
            {work.keywords.map(s => (
              <span key={s} className="keywords" itemProp="keywords">
                {s}
              </span>
            ))}
          </p>
          <p>
            {work.sameAs.map(s => (
              <span key={s} itemProp="keywords">
                {s}
              </span>
            ))}
          </p>
          {work.hasPart && work.hasPart.length && (
            <p>
              Består af:
              {work.hasPart.map(({url}) => (
                <a key={url} itemProp="hasPart" href={url}>
                  {url}
                </a>
              ))}
            </p>
          )}
          {work.isPartOf && work.isPartOf.length && (
            <p>
              {' '}
              Del af:{' '}
              {work.isPartOf.map(({url}) => (
                <React.Fragment>
                  <a key={url} itemProp="isPartOf" href={url}>
                    {url}
                  </a>{' '}
                </React.Fragment>
              ))}
            </p>
          )}
          <p>
            Data som <a href={work.url + '.json'}>schema.org json</a>{' '}
            {work.pid && (
              <a href={work.url + '.json?format=dkabm'}>
                DKABM json-ld
              </a>
            )}
          </p>
        </div>
      </main>
      <hr style={{clear: 'both'}} />
      <h2 style={{textAlign: 'center'}}>Inspiration:</h2>
      <div style={{textAlign: 'justify', margin: '0 5% 0 5%'}}>
        {work.related.map(o => (
          <a
            key={o.wid}
            className="relatedItem"
            href={'https://bibdata.dk/collection/' + o.wid}
          >
            <img src={o.image} />
          </a>
        ))}
      </div>
    </React.Fragment>
  );

  /*
{{#related}}
<a class="relatedItem" href="https://bibdata.dk/collection/{{{wid}}}"><img src="{{{image}}}" /></a> 
{{/related}}
</div>
<p style="margin:20%">
Dette site er en del af <a href="https://bibspire.dk">bibspire.dk</a>. Det bruger DBCs webservices som datakilde, så der kan forekomme data, der kun kan bruges i projekter med biblioteker der abonnerer på disse.
</p>
*/

  return (
    <div
      itemScope="itemScope"
      itemType="http://schema.org/CreativeWork"
    >
      <div id="container">
        <div
          id="view"
          style={{
            display: 'flex',
            flexDirection: 'row',
            height: 100,
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >
          <div style={{display: 'flex'}}>
            <img
              itemProp="image"
              className="mainImg"
              src={''}
              style={{}}
            />
          </div>
        </div>
        <center>
          <div id="metadata">
            <h1 itemProp="name">{work.name}</h1>
            <h2 itemProp="creator">{work.creator.join(' & ')}</h2>
          </div>
        </center>
      </div>
      <div style={{}} />
      <div id="recommendations">
        <div
          style={{
            display: 'inline-block',
            whiteSpace: 'nowrap',
            overflowY: 'scroll'
          }}
        ></div>
      </div>
      <style>{`
          body {
            margin: 0;
            padding: 0;
            font-family: sans-serif;
          }
          #recommendations {
            position: fixed;
            bottom: 0;
            background: black;
            height: 100px;
          }
          #metadata {
            width: 100%;
            max-width: 720px;
            background: white;
          }
          #moreRelated {
            display: inline-block;
            background-image: linear-gradient(to right, 
                rgba(255,255,255,0), 
                rgba(255,255,255,0.9), 
                white);
            line-height: 33px;
            text-align: center;
            position: absolute;
            font-size: 20px;
            font-weight: 100;
            right: 0;
            top: 0;
            height: 100px;
            width: 33px;
            scrollbar-width: none;
          }
          #moreRelated::-webkit-scrollbar {
            display: none;
          }
        `}</style>
    </div>
  );
}

/*
export function render(o) {
  return {
    html: ReactDOMServer.renderToStaticMarkup(<WorkView {...o} />)
  };
}
*/
