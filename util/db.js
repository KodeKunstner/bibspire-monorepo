const Redis = require('ioredis');
const mysql = require('mysql2/promise');

const db = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: 'bibspire',
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
});
async function initDB() {
  await db.execute(`CREATE DATABASE IF NOT EXISTS bibspire`);

  // used by ../util/geoip.js
  await db.execute(`CREATE TABLE IF NOT EXISTS geoip_cache (
    ip INT UNSIGNED NOT NULL,
    location TEXT NOT NULL,
    PRIMARY KEY (ip)
)`);

  // tokens
  await db.execute(`
    CREATE TABLE IF NOT EXISTS tokens (
      token VARCHAR(32) NOT NULL,
      expires TIMESTAMP NOT NULL,
      created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
      reference TEXT DEFAULT "",
      permissions TEXT DEFAULT "{}",
      PRIMARY KEY (token)
    )
  `);

  // used by recommender_adhl.js
  await db.execute(`CREATE TABLE IF NOT EXISTS adhl (
    wid INT NOT NULL,
    d DATE NOT NULL,
    sid INT NOT NULL,
    PRIMARY KEY (wid, d, sid),
    UNIQUE KEY (sid, wid)
)`);
  // used by holding.js
  await db.execute(`CREATE TABLE IF NOT EXISTS holding (
        agency INT NOT NULL,
        wid INT NOT NULL,
        lid INT NOT NULL,
        onshelf INT NOT NULL,
        onloan INT NOT NULL,
        notforloan INT NOT NULL,
        online INT NOT NULL,
        onorder INT NOT NULL,
        PRIMARY KEY (agency, lid),
        KEY (agency, wid)
    );`);
  // used by metadata.js
  await db.execute(`CREATE TABLE IF NOT EXISTS manifests (
    pid VARCHAR(64) NOT NULL,
    json MEDIUMTEXT NULL,
    wid INT NULL,
    PRIMARY KEY (pid),
    KEY (wid, pid)
) CHARACTER SET 'utf8';`);
  // used by recommender_usagedata.js (to be deprecated)
  await db.execute(`CREATE TABLE IF NOT EXISTS usagedata (
    wid INT NOT NULL,
    d DATE NOT NULL,
    sid INT NOT NULL,
    PRIMARY KEY (wid, sid),
    UNIQUE KEY (sid, wid)
)`);
  // used by recommender_webtrekk.js
  await db.execute(`CREATE TABLE IF NOT EXISTS webtrekk (
    wid INT NOT NULL,
    sid INT NOT NULL,
    PRIMARY KEY (wid, sid),
    UNIQUE KEY (sid, wid)
)`);

  // used by recommender.js and holdings.js
  await db.execute(`CREATE TABLE IF NOT EXISTS sites(
    site VARCHAR(255) NOT NULL,
    secret VARCHAR(64) NOT NULL,
    settings MEDIUMTEXT NOT NULL,
    PRIMARY KEY (site)
) CHARACTER SET 'utf8';`);

  // deprecated usage by upload.js
  await db.execute(`CREATE TABLE IF NOT EXISTS holdings (
    agency INT NOT NULL,
    branch INT NOT NULL,
    pid VARCHAR(64) NOT NULL,
    wid INT NOT NULL,
    PRIMARY KEY (branch, pid),
    KEY (agency, pid),
    KEY (agency, wid, pid)
);`);
  // indexed by metadata, will be deprecatead
  await db.execute(`CREATE TABLE IF NOT EXISTS creator_id (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    title VARCHAR(255) NOT NULL,
    PRIMARY KEY (name),
    UNIQUE KEY (id)
) CHARACTER SET 'utf8';`);
  await db.execute(`CREATE TABLE IF NOT EXISTS creator (
    id INT NOT NULL,
    type ENUM('creator', 'contributor') NOT NULL,
    year SMALLINT,
    wid INT NOT NULL,
    PRIMARY KEY (id, type, year, wid),
    UNIQUE KEY (wid, id)
);`);
  for (const table of ['series', 'keywords']) {
    await db.execute(`CREATE TABLE IF NOT EXISTS ${table + '_id'} (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    title VARCHAR(255) NOT NULL,
    PRIMARY KEY (name),
    UNIQUE KEY (id)
) CHARACTER SET 'utf8';`);
    await db.execute(`CREATE TABLE IF NOT EXISTS ${table} (
    id INT NOT NULL,
    wid INT NOT NULL,
    PRIMARY KEY (id, wid),
    KEY (wid, id)
);`);
  }
  // used by metadata, will be deprecated
  await db.execute(`CREATE TABLE IF NOT EXISTS work (
    wid INT NOT NULL,
    name VARCHAR(255) NOT NULL,
    json MEDIUMTEXT NOT NULL,
    PRIMARY KEY (wid),
    KEY (name)
) CHARACTER SET 'utf8';`);
  await db.execute(`CREATE TABLE IF NOT EXISTS works (
    pid VARCHAR(64) NOT NULL,
    wid INT NOT NULL,
    PRIMARY KEY (pid),
    KEY (wid)
);`);
}

module.exports = {
  db,
  dbInitialised: initDB()
};
