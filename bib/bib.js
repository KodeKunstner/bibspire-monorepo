const openplatform = require('./openplatform');
const yaml = require('js-yaml');
const Mustache = require('mustache');
const {
  components: {WorkView}
} = require('../components/components.js');
const ReactDOMServer = require('react-dom/server');

const {
  randomToken,
  normaliseName,
  sleep,
  readFile
} = require('../util/util');
const {db, dbInitialised} = require('../util/db');

const {metadata, manifest, getWork} = require('./metadata');
const {config, recommender} = require('./recommender');
const {holdings, webtrekkUpload, usagedata} = require('./upload');

//
// randomPids
//
let randomPids = [];
(async () => {
  randomPids = (
    await readFile(__dirname + '/randomPids.lst', 'utf-8')
  ).split('\n');
})();
async function randomPid() {
  return randomPids[(Math.random() * randomPids.length) | 0];
}
//
// Swagger
//
function swaggerUI(req, res) {
  res.send(`
<!DOCTYPE html><html><head><meta charset="UTF-8">
<link href="//unpkg.com/swagger-ui-dist@3.22.1/swagger-ui.css" rel="stylesheet" type="text/css"/>
</head><body><div id="swagger-ui"></div>
<script src="//unpkg.com/swagger-ui-dist@3.22.1/swagger-ui-bundle.js"></script>
<script>SwaggerUIBundle({
  url: "/v1/oas3-spec.json",dom_id: '#swagger-ui',
  displayRequestDuration: true,
  presets: [
    SwaggerUIBundle.presets.apis,
    SwaggerUIBundle.SwaggerUIStandalonePreset
  ]})
</script>
</body></html>`);
  return res.end();
}
async function swaggerSpec(req, res) {
  const spec = yaml.safeLoad(
    await readFile(
      __dirname + '/veduz-bibdata-1.1.0-swagger.yaml',
      'utf-8'
    )
  );
  res.json(spec);
  res.end();
}
//
// Cover
//
async function coverHandler(req, res) {
  const pid = req.url.slice(1).replace(/[?./].*/, '');
  const coverUrl = await openplatform.getCover(pid);
  return res.redirect(302, coverUrl);
}
//
// API
//
const endpoints = {
  //loadADHL,
  libraries: async (req, res) =>
    res.json(await openplatform.getLibraries()),
  config,
  randomPid: async (req, res) => res.json(await randomPid()),
  webtrekkUpload,
  recommend: async (req, res) => {
    const t0 = Date.now();
    const {site, pid, limit} = req.query;
    const result = await recommender({site, pid, limit});
    res.json({
      ...result,
      time_taken: Date.now() - t0
    });
  },
  holdings,
  usagedata,
  object: (req, res) => objectHandler({...req, justJSON: true}, res),
  collection: (req, res) =>
    collectionHandler({...req, justJSON: true}, res)
};

function bibHandler(req, res, next) {
  const endpoint = req.url.slice(1).replace(/[?/].*/, '');
  if (endpoint === 'oas3-spec.json') {
    return swaggerSpec(req, res);
  }
  if (endpoint === '') {
    return swaggerUI(req, res);
  }
  if (!req.query.token) {
    res.status(401);
    return res.end('token missing');
  }
  if (endpoints[endpoint]) {
    req.url = decodeURIComponent(req.url.slice(endpoint.length + 1));
    return endpoints[endpoint](req, res);
  }
  res.status(404);
  res.end();
}

//
// HTML rendering of object/collection
//
function objToHtml(obj) {
  const renderedHTML = ReactDOMServer.renderToStaticMarkup(
    WorkView({work: obj})
  );
  const objectHTML =
    `
<!DOCTYPE html><html><head>
<meta charset="UTF-8">
<title>{{name}} - {{creator}}</title>
<link rel="icon" href="{{{image}}}" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta property="og:title" content="{{name}}" />
<meta property="og:type" content="website" />
<meta property="og:url" content="{{{url}}}" />
<meta property="og:image" content="{{{image}}}" />
<meta property="og:description" content="{{description}}" />
<style>
img {
  border:1px solid white;
  box-shadow:2px 2px 5px rgba(0,0,0,0.5);
}
.creator {
  display: inline-block;
  margin-right: 12px;
  font-weight: bold;
}
.keywords {
  display: inline-block;
  margin-right: 12px;
}
.contributor {
  font-weight: normal;
  color: #333;
  font-size: 90%;
}
.relatedItem {
  display: inline-block;
  text-decoration: none;
  color: black;
}
.relatedItem img {
  height: 240px;
  margin: 12px 6px 12px 6px;
}
.info {
  background: #000;
  font-weight: bold;
  color: #fed;
  padding: 12px 4px;
  line-height: 20
  overflow: hidden;
  text-decoration: none;
  text-align: center;
  display: block;
  border-bottom: 1px solid white;
  box-shadow:2px 2px 5px rgba(0,0,0,0.5);
}
</style>
</head>
<body style="margin:0;padding:0;font: 16px sans-serif;background:#fff8f0;">
<a href="/" class=info>
<span style="float:left">Bibliografiske data</span>
<span style="float:right">Danish bibliographic data</span>
bibdata.dk
<br style="clear: both;"/>
</a>

XYZZYSQUAX` /*


<main class="content" style="text-align:center" itemscope itemtype="http://schema.org/{{{@type}}}">
<link itemprop="url" href="{{{url}}}"/>
{{#image}}<img style="margin:20px;" width="280" src="{{{.}}}" />{{/image}}
<div style="display:inline-block;max-width:600px;text-align:left;vertical-align:top;margin: 0 16px 44px 16px;">
<h1 style="clear:none">
  <span itemprop="name">{{name}}</span> 
  {{#alternativeHeadline}}<span class=alternativeHeadline itemprop="alternativeHeadline">{{.}}</span>{{/alternativeHeadline}}
  {{#datePublished}}({{.}}){{/datePublished}}
</h1>
<p class="creatorcontributor">
  {{#creator}}<span class="creator" itemprop="creator">{{.}}</span> {{/creator}}
  {{#contributor}}<span class="creator contributor" itemprop="contributor">{{.}}</span> {{/contributor}}
</p>
{{#description}}<p class="description" itemprop="description">{{.}}</description> {{/description}}
<p>
  {{#keywords}}<span class="keywords" itemprop="keywords">{{.}}</span> {{/keywords}}
</p>
<p>{{#sameAs}}<a itemprop="sameAs" href="{{{.}}}">{{.}}</a>{{/sameAs}}</p>
{{^pid}}
<p>Består af:
{{#hasPart}}<a itemprop="hasPart" href="{{{url}}}">{{url}}</a> {{/hasPart}}
</p>
{{/pid}}
<p>Del af:
{{#isPartOf}}<a itemprop="isPartOf" href="{{{url}}}">{{url}}</a> {{/isPartOf}}
</p>
<p>Data som: <a href="{{{url}}}.json">schema.org json</a> {{#pid}}<a href="{{{url}}}.json?format=dkabm">DKABM json-ld</a>{{/pid}}</p>
</div>
</main>
<hr style="clear:both" />
<h2 style="text-align:center">Inspiration:</h2>
<div style="text-align: justify; margin: 0 5% 0 5%">
{{#related}}
<a class="relatedItem" href="https://bibdata.dk/collection/{{{wid}}}"><img src="{{{image}}}" /></a> 
{{/related}}
</div>
*/ +
    `<p style="margin:20%">
Dette site er en del af <a href="https://bibspire.dk">bibspire.dk</a>. Det bruger DBCs webservices som datakilde, så der kan forekomme data, der kun kan bruges i projekter med biblioteker der abonnerer på disse.
</p>
</body></html>
`;
  return Mustache.render(
    objectHTML.replace('XYZZYSQUAX', renderedHTML),
    obj
  );
}
function wantsHtml(req) {
  if (req.justJSON) {
    return false;
  }
  const extension = req.url
    .replace(/[^.]*[.]?/, '')
    .replace(/[?].*.?/, '');
  const accept = (req.headers.accept || '').toLowerCase();
  const jsonPos = accept.indexOf('json');
  const htmlPos = accept.indexOf('html');
  return (
    extension === 'html' ||
    (extension === '' &&
      htmlPos !== -1 &&
      (jsonPos === -1 || htmlPos < jsonPos))
  );
}
async function objectHandler(req, res, next) {
  const pid = decodeURIComponent(req.url.slice(1)).replace(
    /[?./].*/,
    ''
  );

  if ((req.query.format || '').toLowerCase() === 'dkabm') {
    const dkabm = await metadata(pid);
    delete dkabm._id;
    delete dkabm.wid;
    delete dkabm.hasCover;
    res.json(dkabm);
    return res.end();
  }

  const data = await manifest(pid);
  if (wantsHtml(req)) {
    data.related = (
      await recommender({
        site: 'bibdata.dk',
        pid
      })
    ).related;
    res.send(objToHtml(data));
  } else {
    res.json(data);
  }
  res.end();
}
async function collectionHandler(req, res, next) {
  const wid = req.url.slice(1).replace(/[?./].*/, '');
  const data = await getWork(wid);
  if (wantsHtml(req)) {
    data.related = (
      await recommender({
        site: 'bibdata.dk',
        wid
      })
    ).related;
    res.send(objToHtml(data));
  } else {
    res.json(data);
  }
  res.end();
}
async function rootHandler(req, res) {
  if (req.query.pid) {
    return res.redirect(
      302,
      'https://bibdata.dk/object/' + req.query.pid
    );
  }
  return res.redirect(302, '/object/' + (await randomPid()));
}
async function seriesHandler(req, res, next) {
  res.end();
}

//
// deprecated
//
async function pidHandler(req, res, next) {
  if (req.headers.host.toLowerCase().indexOf('forsider') !== -1) {
    return coverHandler(req, res);
  }
  return objectHandler(req, res);
}

module.exports = {
  rootHandler,
  bibHandler,
  pidHandler,
  objectHandler,
  collectionHandler,
  workHandler: collectionHandler,
  seriesHandler,
  coverHandler
};
