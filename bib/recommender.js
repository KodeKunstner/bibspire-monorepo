const {randomToken, sleep, readFile} = require('../util/util');
const {db} = require('../util/db');

const {manifest, getWork, pid2wid} = require('./metadata');
const webtrekk = require('./recommender_webtrekk');
const usagedata = require('./recommender_usagedata');
const common = require('./recommender_common');
const {availableAgencies, agencyWorkPids} = require('./holding');
const {exactCreator} = require('./elastic');
const {openplatform} = require('./openplatform');

async function getSiteConfig(site) {
  let rowcol = (
    await db.query('SELECT settings FROM sites WHERE site=?;', [site])
  )[0][0];
  let result;
  if (rowcol) {
    try {
      result = JSON.parse(rowcol.settings);
    } catch (e) {
      // pass
      console.log('bib', e);
    }
  } else {
    result = {};
  }
  result = {
    longtailedness: 0.3,
    one_per_creator: true,
    require_cover: true,
    limit: 20,
    ...result
  };
  if (!rowcol) {
    db.execute(
      'INSERT IGNORE INTO sites (site, secret, settings) VALUES (?, ?, ?);',
      [site, randomToken(), JSON.stringify(result)]
    );
  }
  result.client = result.client || {};
  result.client.tingObject = result.client.tingObject || {};
  const tingObject = result.client.tingObject;
  tingObject.rows = tingObject.rows || 3;
  tingObject.coverWidth = tingObject.coverWidth || 158;
  tingObject.parentSelector =
    tingObject.parentSelector || '.ting-object';

  return result;
}
async function config(req, res) {
  const {site, token} = req.query;
  let current = (
    await db.query('SELECT secret FROM sites WHERE site=?;', [site])
  )[0][0];
  if (!current) {
    res.status(404);
    res.send('site parameter not found');
    return res.end();
  }
  const {secret} = current;
  if (token !== secret) {
    res.status(403);
    res.send('invalid toke parameter');
    return res.end();
  }

  if (req.method === 'PUT') {
    let str = '';
    req.on('data', data => (str += data));
    req.on('end', async () => {
      try {
        const settings = JSON.parse(str);
        await db.execute('UPDATE sites SET settings=? WHERE site=?', [
          str,
          site
        ]);
        res.json({ok: true});
        res.end();
      } catch (e) {
        console.log('bib', e);
        res.status(500);
        res.end();
      }
    });
  } else {
    const agencies = await availableAgencies();
    res.json({config: await getSiteConfig(site), agencies});
    res.end();
  }
}
async function recommender({site, pid, wid, limit}) {
  //
  // preprocessing phase
  //
  site = site || 'default';
  site = site.toLowerCase();
  const agencyPromise = db.query(
    'SELECT agency FROM sites WHERE site=?',
    [site]
  );
  const configPromise = getSiteConfig(site);

  if (!Array.isArray(pid)) {
    pid = [pid];
  }

  let preferredType;
  if (!wid) {
    wid = await Promise.all(
      pid.map(async pid => {
        const o = await manifest(pid);
        preferredType = o.additionalType;
        return o.wid;
      })
    );
  } else if (!Array.isArray(wid)) {
    wid = [wid];
  }

  const config = await configPromise;
  let {
    recommenderEngine,
    longtailedness,
    include_ereolen,
    require_cover,
    one_per_creator
  } = config;

  const agency = ((await agencyPromise)[0][0] || {}).agency;
  const sampleSize = 1000;

  //
  //  Recommender phase and re-recommend if no data
  //
  async function recommend() {
    let recommendations;
    if (recommenderEngine === 'dbc') {
      console.time('dbcrecommend ' + pid);
      recommendations = await openplatform.recommend({
        like: pid,
        limit: 24
      });
      console.timeEnd('dbcrecommend ' + pid);
      console.time('resolve pids');
      recommendations = {
        related: await Promise.all(
          recommendations.map(async ({pid, val}) => ({
            wid: await pid2wid(pid),
            weight: val
          }))
        )
      };
      console.timeEnd('resolve pids');
    } else if (recommenderEngine === 'webtrekk') {
      recommendations = await common.recommend(webtrekk, {
        wids: wid,
        longtailedness,
        sampleSize
      });
    } else {
      recommendations = await common.recommend(usagedata, {
        wids: wid,
        longtailedness,
        sampleSize
      });
    }
    return recommendations;
  }

  let recommendations = await recommend();
  if (recommendations.related.length < 3) {
    const work = await getWork(wid[0]);
    const creator = work.creator.concat(work.contributor);
    if (creator[0]) {
      wid = (await exactCreator(creator[0])).slice(0, 5);
      recommendations = await recommend();
    }
  }

  //
  // Postprocessing phase
  //

  let related = recommendations.related;
  related = related.filter(o => !wid.includes(o.wid));
  related = related.slice(0, 200);

  const works = {};
  let promises = related.map(
    async o => (o.work = await getWork(o.wid))
  );

  if (agency) {
    promises = promises.concat(
      related.map(async o => {
        o.holdings = await agencyWorkPids(agency, o.wid);
      })
    );
  }
  await Promise.all(promises);

  if (preferredType) {
    for (const o of related) {
      const parts = o.work.hasPart;
      o.work.hasPart = parts
        .filter(o => o.additionalType === preferredType)
        .concat(parts.filter(o => o.additionalType !== preferredType));
    }
  }

  if (agency) {
    for (const o of related) {
      let parts = o.work.hasPart;
      const pids = o.holdings || [];
      parts = parts.filter(o => {
        return (
          pids.includes(o.pid) ||
          (include_ereolen &&
            o.sameAs.find(s =>
              s.startsWith('https://ereolen.dk/ting/object/')
            ))
        );
      });
      o.work.hasPart = parts;
      o.work.image = '';
      for (const part of parts) {
        if (part.image) {
          o.work.image = part.image;
          break;
        }
      }
    }
    related = related.filter(o => o.work.hasPart.length);
  }

  if (one_per_creator) {
    const creators = {};
    const last = [];
    related = related.filter(o => {
      const creator = o.work.creator[0];
      const keep = !creators[creator];
      creators[creator] = true;
      if (!keep) {
        last.push(o);
      }
      return keep;
    });
    related = related.concat(last);
  }

  if (require_cover) {
    related = related.filter(o => o.work.image);
  }

  if (limit) {
    related = related.slice(0, +limit);
  }
  return {
    related: related.map(o => {
      const {image, name, creator, hasPart} = o.work;
      return {
        wid: o.wid,
        image,
        name,
        creator,
        hasPart: hasPart.map(o => ({
          pid: o.pid,
          datePublished: o.datePublished,
          additionalType: o.additionalType
        }))
      };
    }),
    config: config.client
  };
}

module.exports = {config, recommender};
