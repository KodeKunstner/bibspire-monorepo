const _ = require('lodash');

const {
  sleep,
  cached,
  normaliseName,
  filterDuplicates
} = require('../util/util');
const {db} = require('../util/db');

const openplatform = require('./openplatform');

//
// metadata
//
function workFromManifests(wid, manifests) {
  const url = `https://bibdata.dk/collection/${wid}`;
  const result = {
    '@type': 'http://schema.org/Collection',
    url,
    name: '',
    creator: [],
    image: '',
    contributor: [],
    keywords: [],
    description: [],
    datePublished: [],
    isPartOf: []
  };
  const parts = [];
  const creators = {};
  manifests = _.sortBy(manifests, o => (o.additionalType || '').length);
  for (const metadata of manifests) {
    if (metadata.error) {
      continue;
    }
    const {
      name,
      keywords,
      image,
      creator,
      contributor,
      url,
      datePublished,
      identifier,
      description,
      isPartOf,
      additionalType,
      sameAs,
      pid
    } = metadata;

    if (name) {
      // sometimes names have different suffixes, or is in different case, - so the work name should be the common prefix of the names
      if (!result.name) {
        result.name = name;
      } else {
        let i;
        const n1 = name.toLowerCase();
        const n2 = result.name.toLowerCase();
        for (i = 0; i <= n1.length; ++i) {
          if (n1[i] !== n2[i]) {
            break;
          }
        }
        result.name = name.slice(0, i);
      }
    }

    if (image && !result.image) {
      result.image = image;
    }
    if (
      datePublished &&
      !result.datePublished.includes(datePublished)
    ) {
      result.datePublished.push(datePublished);
    }
    for (const c of creator) {
      creators[c] = (creators[c] || 0) + 1;
    }
    result.keywords = result.keywords.concat(keywords);
    result.contributor = result.contributor.concat(contributor);
    result.contributor = result.contributor.concat(creator);
    for (const s of description) {
      if (
        s.startsWith('Downloades i') ||
        s.startsWith('Indspillet i') ||
        s.startsWith('Beskrivelsen baseret')
      ) {
        continue;
      }
      if ((result.description[0] || '').length < s.length) {
        result.description[0] = s;
      }
    }
    result.isPartOf = result.isPartOf.concat(
      isPartOf.map(o => JSON.stringify(o))
    );
    parts.push({
      '@type': metadata['@type'],
      url,
      image,
      datePublished,
      identifier,
      additionalType,
      sameAs,
      pid
    });
  }

  result.datePublished.sort();
  result.datePublished = [result.datePublished[0]];
  const maxCreatorCount = _.max(Object.values(creators));
  result.creator = Object.keys(creators).filter(
    creator => creators[creator] === maxCreatorCount
  );
  result.contributor = filterDuplicates(result.contributor).filter(
    s => !result.creator.includes(s)
  );
  result.keywords = filterDuplicates(result.keywords);
  result.description = filterDuplicates(result.description);
  result.isPartOf = filterDuplicates(result.isPartOf)
    .map(s => JSON.parse(s))
    .slice(1);

  result.hasPart = parts;
  result.wid = +wid;

  return result;
}
async function metadata(pid) {
  let result;
  pid = String(pid).toLowerCase();
  let o = (
    await db.query('SELECT json FROM manifests WHERE pid=?', [pid])
  )[0][0];
  if (o) {
    if (o.json) {
      result = JSON.parse(o.json);
    }
  } else {
    console.time('openplatform ' + pid);
    let opwork = await openplatform.getWork(pid);
    console.timeEnd('openplatform ' + pid);
    if (opwork.collection)
      opwork.collection = opwork.collection.map(s => s.toLowerCase());
    if (opwork.pid) opwork.pid = opwork.pid.map(s => s.toLowerCase());
    if (
      opwork.dcTitle &&
      opwork.dcTitle[0] &&
      opwork.dcTitle[0].startsWith(
        'Error: unknown/missing/inaccessible record: '
      )
    ) {
      opwork = null;
    }
    await db.execute(
      'INSERT IGNORE INTO manifests (pid, json) VALUES (?, ?);',
      [pid, opwork && JSON.stringify(opwork)]
    );
    result = opwork;
  }
  if (!result) {
    return {error: 'unavailable'};
  }
  if (!result.collection || !result.wid) {
    if (!result.collection) {
      result.collection = [pid];
    }
    if (!result.wid) {
      let wid;
      for (const pid of result.collection) {
        wid = (
          await db.query('SELECT wid FROM works WHERE pid=?;', [pid])
        )[0][0];
        if (wid) break;
      }
      if (wid) {
        result.wid = wid.wid;
      } else {
        await db.execute(
          'INSERT IGNORE INTO works (pid, wid) SELECT ?, COALESCE(MAX(wid) + 1, 1) FROM works;',
          [pid]
        );
        wid = (
          await db.query('SELECT wid FROM works WHERE pid=?;', [pid])
        )[0][0];
        try {
          await db.execute('UPDATE manifests SET wid=? WHERE pid=?;', [
            wid.wid,
            pid
          ]);
        } catch (e) {
          console.log('bib', 'TODO', e);
        }
      }
      if (!wid) {
        throw new Error(`no wid for pid: ${pid}`);
      }
      result.wid = wid.wid;
      if (result.collection) {
        for (const pid of result.collection) {
          await db.execute(
            'INSERT IGNORE INTO works (pid, wid) VALUES (?, ?);',
            [pid, wid.wid]
          );
          try {
            await db.execute(
              'UPDATE manifests SET wid=? WHERE pid=?;',
              [wid.wid, pid]
            );
          } catch (e) {
            console.log('bib', 'TODO', e);
          }
        }
      }
    }
    await db.execute(
      'REPLACE INTO manifests (pid, json, wid) VALUES (?, ?, ?);',
      [pid, JSON.stringify(result), result.wid || null]
    );
  }
  return result || {error: 'unavailable'};
}

async function uncachedManifest(pid) {
  let dkabm;
  pid = decodeURIComponent(pid).trim();
  const folkebibliotekKatalog = /^(100450|131300|135300|700400|710100|714700|715100|715300|715500|715700|715900|716100|716300|716500|716700|716900|717300|717500|718300|718500|718700|719000|720100|721000|721700|721900|722300|723000|724000|725000|725300|725900|726000|726500|726900|727000|730600|731600|732000|732600|732900|733000|733600|734000|735000|736000|737000|737600|739000|740000|741000|742000|743000|744000|745000|746100|747900|748000|748200|749200|751000|753000|754000|755000|756100|757300|757500|758000|760700|761500|762100|763000|765700|766100|766500|767100|770600|770700|771000|772700|773000|774000|774100|774600|775100|775600|776000|776600|777300|777900|778700|779100|781000|781300|782000|782500|784000|784600|784900|785100|786000|790900|900455|911116)-katalog:/;

  pid = pid.replace(folkebibliotekKatalog, '870970-basis:');
  dkabm = await metadata(pid);
  if (dkabm.error) {
    return dkabm;
  }
  const result = {};

  const type =
    {
      article: 'Article',
      audiobook: 'Book',
      book: 'Book',
      game: 'Game',
      'missing type': 'CreativeWork',
      movie: 'Movie',
      music: 'MusicRecording',
      other: 'CreativeWork',
      periodica: 'CreativeWork',
      sheetmusic: 'SheetMusic'
    }[[dkabm.workType || []][0]] || 'CreativeWork';
  result['@type'] = type;

  const title = (dkabm.dcTitleFull ||
    dkabm.titleFull ||
    dkabm.dcTitle ||
    dkabm.title || [''])[0].replace(/[(][^)]*[)]$/, '');
  result.name = title.replace(/ : .*/, '');
  result.alternativeHeadline =
    title.indexOf(' : ') === -1 ? '' : title.split(' : ')[1];
  result.alternateName = dkabm.alternative || [];

  result.image = dkabm.hasCover
    ? `https://bibdata.dk/cover/${dkabm._id}`
    : '';

  // TODO: remove "xx yy" if also "xx yy (...)"
  result.creator = [].concat(
    dkabm.creatorAut || [],
    dkabm.creator || [],
    dkabm.dcCreator || [],
    (dkabm.creatorSort || []).map(inverted => {
      let [name, born] = inverted.split(' (');
      born = born ? ` (${born}` : '';
      const [lastname, firstname] = name.split(', ');
      return `${firstname || ''} ${lastname}${born}`.trim();
    })
  );
  for (const key in dkabm) {
    if (key.startsWith('creator')) {
      if (key !== 'creatorSort') {
        result.creator = result.creator.concat(dkabm[key]);
      }
    }
  }
  result.creator = filterDuplicates(result.creator);

  result.contributor = [];
  for (const key in dkabm) {
    if (key.startsWith('contributor')) {
      result.contributor = result.contributor.concat(dkabm[key]);
    }
  }
  result.contributor = filterDuplicates(
    result.contributor.filter(s => !result.creator.includes(s))
  );

  result.url = `https://bibdata.dk/object/${pid}`;
  result.sameAs = [].concat(
    (dkabm.hasOnlineAccess || []).filter(url => url.startsWith('http'))
  );
  result.sameAs = result.sameAs.concat(dkabm.identifierURI || []);
  result.sameAs = filterDuplicates(result.sameAs);

  result.datePublished = (dkabm.date || [''])[0];

  result.keywords = filterDuplicates(
    [].concat(
      dkabm.subjectDBCF || [],
      dkabm.subjectDBCS || [],
      dkabm.subject || [],
      dkabm.shelfMusicshelf || [],
      dkabm.subjectDBCM || [],
      dkabm.subjectGenre || [],
      dkabm.subjectLCSH || [],
      dkabm.temporalDBCM || [],
      dkabm.temporalDBCP || [],
      dkabm.subjectDK5Text || [],
      (dkabm.subjectDK5 || []).map(s => 'DK5/' + s)
    )
  ).filter(s => s !== 'DK5/sk');

  result.description = [].concat(
    dkabm.abstract || [],
    dkabm.description || []
  );

  result.identifier = [pid.toLowerCase()].concat(
    (dkabm.identifierISBN || []).map(
      s => `isbn:${s.replace(/[^0-9]/g, '')}`
    ),
    (dkabm.identifierISSN || []).map(
      s => `issn:${s.replace(/[^0-9]/g, '')}`
    ),
    dkabm.identifier || []
  );

  result.inLanguage = dkabm.languageISO6392 || [];

  result.subjectOf = filterDuplicates(
    [].concat(
      dkabm.hasReview || [],
      dkabm.discussedIn || [],
      dkabm.hasCreatorDescription || []
    )
  ).map(pid => `https://bibdata.dk/object/${pid}`);

  result.isPartOf = [
    {
      url: `https://bibdata.dk/collection/${dkabm.wid}`
    }
  ].concat(
    (dkabm.descriptionSeries || []).map(s => {
      let [position, series] = s.split(':');
      if (series) {
        series = (series || '').trim();
      } else {
        series = position.trim();
        position = '';
      }
      const result = {
        name: series,
        url: `https://bibdata.dk/series/${normaliseName(series)}`
      };
      if (position) {
        result.position = position.trim();
      }
      return result;
    }),
    (dkabm.titleSeries || []).map(s => {
      const [series, position] = s.split(' ; ');
      const result = {
        name: series,
        url: `https://bibdata.dk/series/${normaliseName(series)}`
      };
      if (position) {
        result.position = position;
      }
      return result;
    }),
    (dkabm.isPartOfManifestation || []).map(pid => ({
      url: `https://bibdata.dk/object/${pid}`
    }))
  );

  result.publisher = (dkabm.publisher || []).join(' ; ');
  result.version = (dkabm.version || []).join(' ; ');

  result.additionalType = (dkabm.typeBibDKType || [''])[0];

  const ages = (dkabm.audienceAge || [])
    .concat(dkabm.subjectDBCN || [])
    .map(s => (s.match(/ ([0-9]+) år/) || [])[1])
    .filter(s => s)
    .map(s => +s);
  result.typicalAgeRange = ages.length
    ? `${Math.min.apply(null, ages)}-`
    : '';

  result.pid = pid;
  result.wid = dkabm.wid;

  return result;
}
const manifest = cached('manifest', 1, uncachedManifest);
async function uncachedGetWork(wid) {
  const pids = (
    await db.query('SELECT pid FROM works WHERE wid=?', [wid])
  )[0];
  const manifests = [];
  for (const {pid} of pids) {
    manifests.push(await manifest(pid));
  }

  const result = workFromManifests(wid, manifests);
  return result;
}
const getWork = cached('work', 1, uncachedGetWork);
const pid2wid = cached(
  'pid2wid',
  1,
  async pid => (await manifest(pid)).wid
);
async function workCount() {
  return (await db.query('SELECT MAX(wid) FROM works'))[0][0][
    'MAX(wid)'
  ];
}
function lid2pid(lid, agency) {
  if (String(lid).length < 8) {
    lid = String(+lid + 100000000).slice(1);
  }
  return '870970-basis:' + lid;
}
module.exports = {
  // TODO: remove metadata export, after bib-cleanup
  metadata,
  manifest,
  getWork,
  pid2wid,
  workCount,
  lid2pid
};

// Rebuild cache/data
async function updateManifestWid() {
  let pids = (await db.query('SELECT pid FROM manifests'))[0].map(
    o => o.pid
  );
  console.log('bib', 'updating ' + pids.length + ' manifests');
  for (let i = 0; i < pids.length; ++i) {
    if (i % 1000 === 0) {
      console.log('bib', 'updateManifestWid', i, pids.length);
    }
    const pid = pids[i];
    await db.query('UPDATE manifests SET wid=? WHERE pid=?', [
      await pid2wid(pid),
      pid
    ]);
  }
}
async function cacheWorks() {
  console.time('cacheWorks()');
  const wc = await workCount();
  for (let wid = 0; wid < wc; ++wid) {
    await getWork(wid);
    if (wid % 1000 === 0) {
      console.log('bib', 'cacheWorks', wid, wc);
    }
  }
  console.timeEnd('cacheWorks()');
}
async function updateCache() {
  cacheWorks();
  updateManifestWid();
}
//setTimeout(updateCache, 1000);
